package com.ex2_5;

public class Main {
    public static void main(String[] args) {
        Account acc = new Account(1, new Customer(145, "dipto", 'm'), 1000.00);
        acc.deposit(500.00);
        System.out.println(acc.getCustomerName()+"'s Balane after deposit of 500 taka: "+acc.getBalance());
        System.out.println("Account ID: "+acc.getId());
        System.out.println("Customer info: "+acc.getCustomer());
        System.out.println("Balance after withdrawn 3500 taka: "+acc.withdraw(3500.00));
        Account acc2 = new Account(2, new Customer(376, "someone", 'f'));
        acc2.setBalance(3400.57);
        System.out.println(acc2.toString());
        System.out.println("After withdrawn 500 taka: "+acc2.withdraw(500.00));


    }
}
