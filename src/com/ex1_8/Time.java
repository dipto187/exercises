package com.ex1_8;

public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public void setTime(int hour, int minute, int second){
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public String toString(){
        String h = String.format("%02d",hour);
        String m = String.format("%02d", minute);
        String s = String.format("%02d",second);
        return h+":"+m+":"+s;
    }

    public Time nextSecond(){
        second++;
        if(second>=60){
            second = 0;
            minute++;
        }
        if(minute>=60){
            minute = 0;
            hour++;
        }
        if(hour>=24){
            hour = 0;
        }
        return this;
    }

    public Time previousSecond(){
        second--;
        if(second<0){
            second = 59;
            minute--;
        }
        if(minute<0){
            minute = 59;
            hour--;
        }
        if(hour<0){
            hour = 23;
        }
        return this;
    }
}
