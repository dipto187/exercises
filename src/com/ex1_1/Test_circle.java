package com.ex1_1;

public class Test_circle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("The circle has radius of "+c1.getRadius()+" and area of "+c1.getArea());
        Circle c2 = new Circle(2.0);
        System.out.println("The circle has radius of "+c2.getRadius()+" and area of "+c2.getArea());

        Circle c3 = new Circle();
        c3.setRadius(5.5);
        System.out.println("radius is: " + c3.getRadius());
        c3.setColor("green");
        System.out.println("color is: " + c3.getColor());

        Circle c6 = new Circle(6.6);
        System.out.println(c6.toString());
        System.out.println(c6);
        System.out.println("Operator '+' invokes toString() too: " + c6);
    }
}
