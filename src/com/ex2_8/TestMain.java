package com.ex2_8;

public class TestMain {
    public static void main(String[] args) {
        MyPoint m1 = new MyPoint(7,8);
        MyCircle c1 = new MyCircle();
        MyCircle c2 = new MyCircle(7,8,4);
        MyCircle c3 = new MyCircle(m1, 4);
        System.out.println("Circle c1: "+ c1);
        System.out.println("Circle c2: "+ c2);
        System.out.println("Circle c2: "+ c2);
        System.out.println("Circle c3: "+ c3);
        c1.setRadius(9);
        System.out.println(c1.getRadius());

        c2.setCenterXY(3, 3);
        System.out.println(c2.getCenter());

        c1.setCenter(new MyPoint(4,4));
        System.out.println(c1.getCenter());
        System.out.println(c3.getArea());
        System.out.println(c3.getCircumference());
        System.out.println(c2.distance(c3));

    }
}
