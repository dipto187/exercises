package com.ex2_10;

public class MyRectangle {
    private MyPoint p1;
    private MyPoint p2;

    public MyRectangle(int x1, int y1, int x2, int y2){
        p1 = new MyPoint(x1, y1);
        p2 = new MyPoint(x2, y2);
    }

    public MyRectangle(MyPoint p1, MyPoint p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public MyPoint getP1() {
        return p1;
    }

    public void setP1(MyPoint p1) {
        this.p1 = p1;
    }

    public MyPoint getP2() {
        return p2;
    }

    public void setP2(MyPoint p2) {
        this.p2 = p2;
    }

    public double getArea(){
        return Math.abs((p2.getX()-p1.getX())*(p2.getY()-p1.getY()));
    }

    public double getPerimeter(){
        return Math.abs(2*((p2.getX()-p1.getX()) + (p2.getY()-p1.getY())));
    }

    public String toString(){
        return "Rectangle[p1="+p1.toString()+", p2="+p2.toString()+"]";
    }
}
