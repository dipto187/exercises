package com.ex2_10;

public class TestMain {
    public static void main(String[] args) {
        MyRectangle rec1 = new MyRectangle(2,7,1,9);
        MyRectangle rec2 = new MyRectangle(new MyPoint(5,8), new MyPoint(9,12));

        System.out.println(rec1.toString()+"Area="+rec1.getArea());
        System.out.println(rec2.toString()+"Area="+rec2.getArea());
        System.out.println("Perimeter of rec1="+rec1.getPerimeter());
        System.out.println("Perimeter of rec2="+rec2.getPerimeter());

    }
}
