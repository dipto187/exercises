package com.ex2_7;

public class MyLine {
    private MyPoint begin;
    private MyPoint end;

    public MyLine(int x1, int y1, int x2, int y2) {
        this.begin = new MyPoint(x1, y1);
        this.end = new MyPoint(x2, y2);
    }
    public MyLine(MyPoint begin, MyPoint end) {
        this.begin = begin;
        this.end = end;
    }
    public void setBegin(MyPoint begin) {
        this.begin = begin;
    }
    public void setEnd(MyPoint end) {
        this.end = end;
    }
    public MyPoint getBegin() {
        return this.begin;
    }
    public MyPoint getEnd() {
        return this.end;
    }
    public int getBeginX() {
        return this.begin.getX();
    }
    public void setBeginX(int x) {
        this.begin.setX(x);
    }
    public int getBeginY() {
        return this.begin.getY();
    }
    public void setBeginY(int y) {
        this.begin.setY(y);
    }
    public int getEndX() {
        return this.end.getX();
    }
    public void setEndX(int x) {
        this.end.setX(x);
    }
    public int getEndY() {
        return this.end.getY();
    }
    public void setEndY(int y) {
        this.end.setY(y);
    }
    public int[] getBeginXY() {
        return new int[] {this.begin.getX(), this.begin.getY()};
    }
    public int[] getEndXY() {
        return new int[] {this.end.getX(), this.end.getY()};
    }
    public void setBeginXY(int x, int y) {
        this.begin.setXY(x, y);
    }
    public void setEndXY(int x, int y) {
        this.end.setXY(x, y);
    }
    public double getLength() {
        return Math.sqrt(Math.pow(this.begin.getX() - this.end.getX(), 2) + Math.pow(this.begin.getY() - this.end.getY(), 2));
    }
    public double getGradient() {
        return Math.atan2(this.begin.getY(), this.end.getY()) - Math.atan2(this.begin.getX(), this.begin.getX());
    }

    public String toString() {
        return "MyLine[begin=(" + this.begin.getX() + ", " + this.begin.getY() + "),end=(" + this.end.getX() + "," + this.end.getY() + ")]";
    }

}
